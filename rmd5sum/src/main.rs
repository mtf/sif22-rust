use clap::Parser;
#[derive(Parser, Debug)]
#[clap(about, long_about = None)]
struct Args {
    #[clap(value_name = "FILE")]
    files: Vec<String>,
}

use rmd5sum::{MD5Hash, MD5State};
use std::fs::File;
use std::io::Read;

fn main() {
    let args = Args::parse();
    let files = if args.files.is_empty() {
        vec![String::from("-")]
    } else {
        args.files
    };
    for path in files {
        let res = if path == "-" {
            hash_readable(&mut std::io::stdin())
        } else {
            match File::open(&path) {
                Err(err) => Err(err),
                Ok(mut file) => hash_readable(&mut file),
            }
        };
        match res {
            Err(err) => eprintln!("{}: {}", path, err),
            Ok(hash) => println!("{}  {}", hash, path),
        }
    }
}

fn hash_readable<R>(reader: &mut R) -> Result<MD5Hash, std::io::Error>
where
    R: Read,
{
    // Your code here
    unimplemented!()
}
