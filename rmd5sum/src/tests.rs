use super::*;

fn test_string(s: &str, hash: &str) {
    let mut md5state = MD5State::new();
    md5state.hash_bytes(s.as_bytes());
    let md5hash = md5state.finish();
    assert_eq!(&md5hash.to_string(), hash);
}

#[test]
fn empty_string() {
    test_string("", "d41d8cd98f00b204e9800998ecf8427e")
}

#[test]
fn test1() {
    test_string(
        "The quick brown fox jumps over the lazy dog",
        "9e107d9d372bb6826bd81d3542a419d6",
    )
}

#[test]
fn test2() {
    test_string(
        "The quick brown fox jumps over the lazy dog.",
        "e4d909c290d0fb1ca068ffaddf22cbd0",
    )
}

#[test]
fn test3() {
    test_string(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "db89bb5ceab87f9c0fcc2ab36c189c2c",
    )
}

#[test]
fn test4() {
    test_string(
        r#"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Diam vulputate ut pharetra sit amet aliquam id diam. Arcu risus quis varius quam quisque id. Consectetur lorem donec massa sapien faucibus et. Scelerisque felis imperdiet proin fermentum leo vel orci. Accumsan in nisl nisi scelerisque eu. Nisi vitae suscipit tellus mauris a diam maecenas sed enim. Scelerisque purus semper eget duis at tellus at urna. Velit egestas dui id ornare. Cras ornare arcu dui vivamus. Amet porttitor eget dolor morbi non arcu risus quis. Diam phasellus vestibulum lorem sed risus ultricies. Ut porttitor leo a diam. Enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit. Faucibus in ornare quam viverra orci sagittis eu. Lorem ipsum dolor sit amet. Et ligula ullamcorper malesuada proin.

Elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi. Diam vel quam elementum pulvinar etiam non quam lacus. Turpis egestas integer eget aliquet nibh praesent tristique. Tristique senectus et netus et malesuada. Lacus laoreet non curabitur gravida arcu ac tortor. Tempor orci eu lobortis elementum nibh tellus molestie nunc non. Proin fermentum leo vel orci porta non pulvinar neque. Et netus et malesuada fames ac turpis egestas. Fusce ut placerat orci nulla pellentesque dignissim enim sit. Sed viverra ipsum nunc aliquet bibendum enim facilisis gravida."#,
        "349daa3f43ab2ce918ee1cdd641c34b1",
    )
}
