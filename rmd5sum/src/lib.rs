use std::fmt::{self, Display, Formatter};

#[derive(Debug)]
pub struct MD5Hash([u8; 16]);

impl Display for MD5Hash {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        for b in self.0 {
            write!(f, "{:02x}", b)?
        }
        Ok(())
    }
}

#[allow(clippy::zero_prefixed_literal)]
const S: [u32; 64] = [
    07, 12, 17, 22, 07, 12, 17, 22, 07, 12, 17, 22, 07, 12, 17, 22, // 00..16
    05, 09, 14, 20, 05, 09, 14, 20, 05, 09, 14, 20, 05, 09, 14, 20, // 16..32
    04, 11, 16, 23, 04, 11, 16, 23, 04, 11, 16, 23, 04, 11, 16, 23, // 32..48
    06, 10, 15, 21, 06, 10, 15, 21, 06, 10, 15, 21, 06, 10, 15, 21, // 48..64
];

const K: [u32; 64] = [
    0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee, // 00..04
    0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501, // 04..08
    0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be, // 08..12
    0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821, // 12..16
    0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa, // 16..20
    0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8, // 20..24
    0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed, // 24..28
    0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a, // 28..32
    0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c, // 32..36
    0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70, // 36..40
    0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05, // 40..44
    0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665, // 44..48
    0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039, // 48..52
    0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1, // 54..56
    0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1, // 56..60
    0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391, // 60..64
];

const A0: u32 = 0x67452301;
const B0: u32 = 0xefcdab89;
const C0: u32 = 0x98badcfe;
const D0: u32 = 0x10325476;

#[derive(Debug)]
pub struct MD5State {
    // Your code here
}

impl MD5State {
    pub fn new() -> Self {
        // Your code here
        unimplemented!()
    }
    pub fn hash_bytes(&mut self, bytes: &[u8]) {
        // Your code here
        unimplemented!()
    }
    pub fn finish(self) -> MD5Hash {
        // Your code here
        unimplemented!()
    }
}

impl Default for MD5State {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests;
